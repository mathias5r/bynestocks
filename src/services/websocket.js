import {STOCKS_SERVER} from '../constants';
import {store} from '../redux/store';
import {setSystemStocks} from '../redux/actions';
import ReconnectingWebSocket from 'reconnecting-websocket';
import {
  mergeStockInfo,
  updateUserStocks,
  updateGraphData,
} from '../services/stock';
import {handleStocksBuffer} from '../services/buffer';
import logger from '../services/logger';

const ws = new ReconnectingWebSocket(STOCKS_SERVER, [], {
  maxRetries: 15,
  maxReconnectionDelay: 20000,
});

const handleConnect = ({supportedSymbols, stocksData}) => {
  const userStocks = store.getState().User.userStocks;
  mergeStockInfo({userStocks, dispatch: store.dispatch, stocksData});
  store.dispatch(setSystemStocks({supportedSymbols, stocksData}));
};

const handleStockUpdate = ({stocks}) => {
  const key = Object.keys(stocks)[0];
  const value = Number(stocks[key]);
  const userStocks = store.getState().User.userStocks || {};
  const stocksBuffer = store.getState().User.stocksBuffer || {};
  const avgValue = handleStocksBuffer({
    userStocks,
    stocksBuffer,
    key,
    value,
    dispatch: store.dispatch,
  });
  if (!avgValue) {
    return;
  }
  updateUserStocks({
    userStocks,
    key,
    value: avgValue,
    dispatch: store.dispatch,
  });
  const graphData = store.getState().System.graphData;
  updateGraphData({
    userStocks,
    key,
    value: avgValue,
    graphData,
    dispatch: store.dispatch,
  });
};

const HANDLERS = {
  connected: ({supportedSymbols, stocksData}) =>
    handleConnect({supportedSymbols, stocksData}),
  'stocks-update': ({stocks}) => handleStockUpdate({stocks}),
};

const sendMessage = (message) => {
  ws.send(JSON.stringify(message));
};

ws.onopen = () => {
  const userStocks = store.getState().User.userStocks;
  sendMessage({event: 'subscribe', stocks: Object.keys(userStocks)});
};

ws.onmessage = (e) => {
  const data = JSON.parse(e.data);
  const event = data.event;
  if (HANDLERS.hasOwnProperty(event)) {
    return HANDLERS[event](data);
  }
  logger.error('Unhandled event: ', event);
};

ws.onerror = (e) => {
  logger.error(e.message);
};

ws.onclose = (e) => {
  logger.info('close:', e);
};

export default {
  sendMessage,
};
