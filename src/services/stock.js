import {setUserStocks, setGraphData} from '../redux/actions';
import ws from '../services/websocket';

export const mergeStockInfo = ({userStocks, dispatch, stocksData}) => {
  const userStocksData = stocksData.filter((stock) =>
    userStocks.hasOwnProperty(stock.symbol),
  );
  userStocksData.forEach(
    (data) => (userStocks[data.symbol] = {...userStocks[data.symbol], ...data}),
  );
  dispatch(setUserStocks({...userStocks}));
};

export const deleteUserStock = ({userStocks, dispatch, stockCode}) => {
  const stocks = Object.keys(userStocks).reduce((object, key) => {
    if (key !== stockCode) {
      object[key] = userStocks[key];
    }
    return object;
  }, {});
  dispatch(setUserStocks(stocks));
  if (Object.keys(stocks).length === 0) {
    dispatch(setGraphData([0, 0, 0]));
  }
  ws.sendMessage({event: 'unsubscribe', stocks: [stockCode]});
};

export const reorderList = ({userStocks, stockCode, dispatch}) => {
  const stock = userStocks[stockCode];
  const newUserStocks = {[stockCode]: stock, ...userStocks};
  dispatch(setGraphData([0, 0, 0]));
  dispatch(setUserStocks({...newUserStocks}));
};

export const getStockStatus = ({old, value}) => {
  if (old > value) {
    return 'down';
  }
  if (old < value) {
    return 'up';
  }
  return 'normal';
};

export const updateUserStocks = ({userStocks, key, value, dispatch}) => {
  if (userStocks[key]) {
    const old = userStocks[key].value;
    userStocks[key].value = value;
    userStocks[key].status = getStockStatus({old, value});
    dispatch(setUserStocks({...userStocks}));
  }
};

export const updateGraphData = ({
  userStocks,
  key,
  value,
  graphData,
  dispatch,
}) => {
  const first = Object.keys(userStocks)[0];
  if (key === first) {
    if (graphData.length < 100) {
      graphData.push(value);
    } else {
      graphData.shift();
      graphData.push(value);
    }
  }
  dispatch(setGraphData([...graphData]));
};

export const createUserStock = ({
  userStocks,
  dispatch,
  newStock,
  stocksData,
  setShowErrorModal,
  supportedSymbols,
}) => {
  if (!supportedSymbols.includes(newStock)) {
    setShowErrorModal(true);
    return;
  }
  const stocks = {
    ...userStocks,
    [newStock]: {},
  };
  mergeStockInfo({userStocks: stocks, dispatch, stocksData});
  ws.sendMessage({event: 'subscribe', stocks: Object.keys(stocks)});
};
