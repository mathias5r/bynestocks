const error = (text) => {
  // console.log('ERROR:', text);
};

const info = (text) => {
  // console.log('INFO:', text);
};

export default {
  error,
  info,
};
