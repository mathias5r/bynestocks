export const STOCKS_SERVER = process.env.STOCKS_SERVER || 'ws://localhost:8080';
import {Dimensions} from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const TEXTS = {
  INTRO: 'Já olhou suas ações hoje?',
  INSTRUCTION: 'Adicione acime o código do seu ativo',
  CLOSE: 'fechar',
  STOCK_NOT_EXITS: 'Código de ativo não existe',
};

export const STATUS = {
  up: 'green',
  down: 'red',
  normal: 'gray',
};
