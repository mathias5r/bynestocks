import {
  SET_USER_STOCKS,
  SET_SYSTEM_STOCKS,
  SET_SYSTEM_GRAPH,
  SET_STOCKS_BUFFER,
} from './actions';

const userInitialState = {
  userStocks: {},
  stocksBuffer: {},
};

export const User = (state = userInitialState, action) => {
  switch (action.type) {
    case SET_USER_STOCKS:
      return {
        ...state,
        userStocks: action.userStocks,
      };
    case SET_STOCKS_BUFFER:
      return {
        ...state,
        stocksBuffer: action.stocksBuffer,
      };
    default:
      return state;
  }
};

const systemInitialState = {
  systemStocks: {},
  graphData: [],
};

export const System = (state = systemInitialState, action) => {
  switch (action.type) {
    case SET_SYSTEM_STOCKS:
      return {
        ...state,
        systemStocks: action.systemStocks,
      };
    case SET_SYSTEM_GRAPH:
      return {
        ...state,
        graphData: action.graphData,
      };
    default:
      return state;
  }
};
