export const SET_USER_STOCKS = 'SET_USER_STOCKS';
export const SET_SYSTEM_STOCKS = 'SET_SYSTEM_STOCKS';
export const SET_SYSTEM_GRAPH = 'SET_SYSTEM_GRAPH';
export const SET_STOCKS_BUFFER = 'SET_STOCKS_BUFFER';

export const setUserStocks = (value) => ({
  type: SET_USER_STOCKS,
  userStocks: value,
});

export const setSystemStocks = (value) => ({
  type: SET_SYSTEM_STOCKS,
  systemStocks: value,
});

export const setGraphData = (value) => ({
  type: SET_SYSTEM_GRAPH,
  graphData: value,
});

export const setStocksBuffer = (value) => ({
  type: SET_STOCKS_BUFFER,
  stocksBuffer: value,
});
