import React from 'react';
import styled from 'styled-components';
import {useSelector} from 'react-redux';
import {LineChart, Grid, YAxis} from 'react-native-svg-charts';
import {SCREEN_HEIGHT} from '../constants';

const Container = styled.View`
  flex: 1;
  flex-direction: row;
`;

const Axis = styled(YAxis)`
  height: ${SCREEN_HEIGHT * 0.15}px;
`;

const Graph = styled(LineChart)`
  flex: 1;
`;

const contentInset = {top: 20, bottom: 20};

const GraphComponent = () => {
  const graphData = useSelector((state) => state.System.graphData);
  return (
    <Container>
      <Axis
        data={graphData}
        contentInset={contentInset}
        svg={{
          fill: 'white',
          fontSize: 10,
        }}
        numberOfTicks={5}
        formatLabel={(value) => `${value}`}
      />
      <Graph
        data={graphData}
        svg={{stroke: 'rgb(255, 255, 255)'}}
        contentInset={contentInset}
        xMax={200}>
        <Grid />
      </Graph>
    </Container>
  );
};

export default GraphComponent;
