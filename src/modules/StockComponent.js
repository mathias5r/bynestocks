import React from 'react';
import styled from 'styled-components';
import {SCREEN_HEIGHT, STATUS} from '../constants';
import shadow from '../styles/shadow';
import {CenterView} from '../styles/view';

const Container = styled.TouchableOpacity`
  width: 100%;
  height: ${SCREEN_HEIGHT * 0.12}px;
  flex-direction: row;
  margin: 10px 0;
  padding: 0 20px;
`;

const SymbolView = styled(CenterView)`
  height: ${SCREEN_HEIGHT * 0.12}px;
  width: ${SCREEN_HEIGHT * 0.12}px;
  background-color: white;
  border-radius: 7px;
  margin: 0 10px;
`;

const Text = styled.Text`
  font-size: 18px;
`;

const Symbol = styled(Text)`
  font-weight: bold;
`;

const Value = styled.Text`
  font-size: 16px;
  color: ${({status}) => STATUS[status]};
`;

const StaticView = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const Name = styled.Text`
  font-size: 14px;
  color: gray;
`;

const Base = styled(Name)`
  font-weight: bold;
`;

const InfoView = styled.View`
  flex: 1;
  background-color: white;
  border-radius: 7px;
  margin: 0 10px 0 5px;
  padding: 10px;
  justify-content: space-between;
`;

const StockComponent = ({
  stock,
  value,
  status,
  basePrice,
  companyName,
  onLongPress,
  onPress,
}) => (
  <Container
    activeOpacity={1}
    onLongPress={() => onLongPress(stock)}
    onPress={() => onPress(stock)}>
    <SymbolView style={shadow}>
      <Symbol>{stock || ''}</Symbol>
    </SymbolView>
    <InfoView style={shadow}>
      <Value status={status || 'normal'}>
        {String(value || '').substring(0, 5)}
      </Value>
      <StaticView>
        <Name>{companyName}</Name>
        <Base>{String(basePrice || '').substring(0, 5)}</Base>
      </StaticView>
    </InfoView>
  </Container>
);

export default StockComponent;
