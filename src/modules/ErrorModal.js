import React from 'react';
import {Modal} from 'react-native';
import styled from 'styled-components';
import shadow from '../styles/shadow';
import {TEXTS} from '../constants';
import colors from '../styles/colors';

const Center = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const Body = styled.View`
  height: 20%;
  width: 80%;
  background-color: white;
  border-radius: 25px;
  justify-content: center;
  align-items: center;
`;

const CloseButton = styled.TouchableOpacity`
  background-color: ${colors.main};
  padding: 1px;
  border-radius: 25px;
`;

const Text = styled.Text`
  font-size: 16px;
  padding: 10px;
`;

const ButtonText = styled(Text)`
  color: white;
`;

const ErrorModal = ({error, visible, setVisible}) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => setVisible(false)}>
      <Center>
        <Body style={shadow}>
          <Text>{error}</Text>
          <CloseButton onPress={() => setVisible(false)}>
            <ButtonText>{TEXTS.CLOSE}</ButtonText>
          </CloseButton>
        </Body>
      </Center>
    </Modal>
  );
};

export default ErrorModal;
