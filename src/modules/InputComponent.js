import React from 'react';
import styled from 'styled-components';
import {SCREEN_HEIGHT} from '../constants';
import shadow from '../styles/shadow';
import AddIcon from '../assets/add_icon.png';
import {CenterView} from '../styles/view';

const Container = styled(CenterView)`
  width: 100%;
  height: ${SCREEN_HEIGHT * 0.1}px;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
  position: absolute;
  top: 0;
  z-index: 3;
  background-color: white;
  padding: 0 20px;
  flex-direction: row;
`;

const Input = styled.TextInput`
  width: 80%;
`;

const Button = styled.TouchableOpacity``;

const Icon = styled.Image`
  height: 25px;
  width: 50px;
`;

const InputComponent = ({value, onChangeText, onButtomPress}) => (
  <Container style={shadow}>
    <Input
      value={value}
      onChangeText={onChangeText}
      placeholder="Código do Ativo"
      maxLength={3}
    />
    <Button onPress={onButtomPress}>
      <Icon source={AddIcon} />
    </Button>
  </Container>
);

export default InputComponent;
