import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import StockComponent from './StockComponent';
import styled from 'styled-components';
import InputComponent from './InputComponent';
import HeaderComponent from './HeaderComponet';
import {deleteUserStock, createUserStock, reorderList} from '../services/stock';
import {SCREEN_HEIGHT, TEXTS} from '../constants';
import intro from '../assets/intro.png';
import {scrollView} from '../styles/view';
import ErrorModal from './ErrorModal';
import {CenterView} from '../styles/view';
import colors from '../styles/colors';
import {Keyboard} from 'react-native';

const SafeView = styled.SafeAreaView`
  width: 100%;
  height: 100%;
  background-color: ${colors.main};
`;

const Container = styled.View`
  flex: 1;
`;

const ContentView = styled.SafeAreaView`
  height: 100%;
  justify-content: flex-end;
  align-items: center;
  padding-top: 20px;
  background-color: white;
`;

const StocksView = styled.FlatList`
  background-color: transparent;
  width: 100%;
  height: 100%;
  margin-top: 200px;
  z-index: 2;
`;

const IntroView = styled(CenterView)`
  flex: 1;
  background-color: white;
`;

const IntroImage = styled.Image`
  width: ${SCREEN_HEIGHT * 0.3}px;
  height: ${SCREEN_HEIGHT * 0.3}px;
`;

const IntroText = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: ${colors.main};
`;

const IntroSubText = styled(IntroText)`
  font-size: 16px;
  font-weight: normal;
`;

const HomeComponent = ({}) => {
  const userStocks = useSelector((state) => state.User.userStocks);
  const {stocksData, supportedSymbols} = useSelector(
    (state) => state.System.systemStocks,
  );
  const dispatch = useDispatch();
  const [showErrorModal, setShowErrorModal] = useState(false);
  const [typedStock, setTypedStock] = useState('');
  return (
    <SafeView>
      <Container>
        {!Object.keys(userStocks).length ? (
          <IntroView>
            <IntroImage source={intro} contain="contain" />
            <IntroText>{TEXTS.INTRO}</IntroText>
            <IntroSubText>{TEXTS.INSTRUCTION}</IntroSubText>
          </IntroView>
        ) : (
          <ContentView>
            <HeaderComponent />
            <StocksView
              contennt
              data={Object.keys(userStocks)}
              keyExtractor={(_, index) => index.toString()}
              renderItem={({item}) => (
                <StockComponent
                  contentContainerStyle={scrollView}
                  stock={item}
                  {...userStocks[item]}
                  onLongPress={(stockCode) =>
                    deleteUserStock({userStocks, dispatch, stockCode})
                  }
                  onPress={(stockCode) =>
                    reorderList({userStocks, stockCode, dispatch})
                  }
                />
              )}
            />
          </ContentView>
        )}
        <InputComponent
          value={typedStock}
          onChangeText={(text) => setTypedStock(text.toUpperCase())}
          onButtomPress={() => {
            Keyboard.dismiss();
            setTypedStock('');
            createUserStock({
              userStocks,
              dispatch,
              newStock: typedStock,
              stocksData,
              setShowErrorModal,
              supportedSymbols,
            });
          }}
        />
        {showErrorModal && (
          <ErrorModal
            error={TEXTS.STOCK_NOT_EXITS}
            visible
            setVisible={() => setShowErrorModal(false)}
          />
        )}
      </Container>
    </SafeView>
  );
};

export default HomeComponent;
