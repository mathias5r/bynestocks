import React from 'react';
import styled from 'styled-components';
import {SCREEN_HEIGHT} from '../constants';
import {useSelector} from 'react-redux';
import GraphComponent from './GraphComponent';

const Container = styled.View`
  height: ${SCREEN_HEIGHT * 0.4}px;
  width: 100%;
  position: absolute;
  top: 0;
  background-color: rgb(105, 191, 191);
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
  z-index: 1;
  justify-content: center;
`;

const ContentView = styled.View`
  height: ${SCREEN_HEIGHT * 0.2}px;
  width: 100%;
  flex-direction: row;
  padding-top: 20px;
  align-items: center;
`;

const GraphView = styled.View`
  flex: 0.5;
  flex-direction: row;
  padding-right: 25px;
`;

const InfoView = styled.View`
  flex: 0.5;
  padding-left: 25px;
`;

const Text = styled.Text`
  color: white;
  font-size: 16px;
`;

const Symbol = styled(Text)`
  font-size: 20px;
  font-weight: bold;
`;

const Subtext = styled(Text)`
  font-size: 12px;
`;

const HeaderComponent = () => {
  const userStocks = useSelector((state) => state.User.userStocks);
  const keys = Object.keys(userStocks);
  const {symbol, companyName, catchPhrase} = userStocks[keys[0]] || {};
  if (!Object.keys(userStocks).length) {
    return <></>;
  }
  return (
    <Container>
      <ContentView>
        <InfoView>
          <Symbol>{symbol}</Symbol>
          <Text>{companyName}</Text>
          <Subtext>{catchPhrase}</Subtext>
        </InfoView>
        <GraphView>
          <GraphComponent />
        </GraphView>
      </ContentView>
    </Container>
  );
};

export default HeaderComponent;
