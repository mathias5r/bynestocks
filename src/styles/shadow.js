import {StyleSheet, Platform} from 'react-native';

const styles = StyleSheet.create({
  ios: {
    shadowColor: '#000',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.22,
    shadowRadius: 11,
  },
  android: {
    elevation: 2,
  },
});

export default styles[Platform.OS];
