import styled from 'styled-components';

export const scrollView = {
  alignItems: 'center',
};

export const CenterView = styled.View`
  justify-content: center;
  align-items: center;
`;
