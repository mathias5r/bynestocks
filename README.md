# Byne Stocks

React Native app to monitor stocks market

![](video.gif)

## Requirements

React Native CLI

```bash
yarn global add react-native-cli
```
CocoaPods (for ios)

```bash
gem install cocoapods
```

## Installation

```bash
# in root folder
yarn
cd ios && pod install
```

## Environment

in *./src/constants* there is the endpoint config:

```javacript
process.env.STOCKS_SERVER || 'ws://localhost:8080';
```

## Running 

```bash
react-native run-ios
react-native run-android
```

## Testing
```bash
yarn test
```

## Libraries used
* [reconnecting-websocket](https://github.com/joewalnes/reconnecting-websocket) - websocket reconnection with backoff algorithm
* [react-native-svg-charts](https://github.com/JesperLekland/react-native-svg-charts) - draw graphs
