import {
  createUserStock,
  deleteUserStock,
  reorderList,
  getStockStatus,
  updateGraphData,
  updateUserStocks,
} from '../../src/services/stock';

const mockedUserStocks = {
  IET: {
    value: 100,
  },
  T: {
    value: 120,
  },
};

const mockedStockData = [
  {symbol: 'IET', companyName: 'Teste1'},
  {symbol: 'T', companyName: 'Teste2'},
  {symbol: 'N', companyName: 'Teste3'},
];

const mockedSupportedSymbols = ['IET', 'T', 'N'];

jest.mock('../../src/services/websocket', () => ({
  sendMessage: jest.fn(),
}));

describe('Test stock service', () => {
  describe('Test createUserStock method', () => {
    let dispatch = jest.fn();
    let setShowErrorModal = jest.fn();

    beforeEach(() => {
      dispatch = jest.fn();
      setShowErrorModal = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should not create stock when stock does not exists', () => {
      // act
      createUserStock({
        userStocks: mockedUserStocks,
        dispatch,
        newStock: 'AAA',
        stocksData: mockedStockData,
        setShowErrorModal,
        supportedSymbols: mockedSupportedSymbols,
      });

      // expect
      expect(setShowErrorModal).toBeCalledWith(true);
      expect(dispatch).not.toBeCalled();
    });
    test('Should create stock with stock data', () => {
      // act
      createUserStock({
        userStocks: mockedUserStocks,
        dispatch,
        newStock: 'N',
        stocksData: mockedStockData,
        setShowErrorModal,
        supportedSymbols: mockedSupportedSymbols,
      });

      // expect
      expect(dispatch).toBeCalledWith({
        type: 'SET_USER_STOCKS',
        userStocks: {
          IET: {
            companyName: 'Teste1',
            symbol: 'IET',
            value: 100,
          },
          N: {
            companyName: 'Teste3',
            symbol: 'N',
          },
          T: {
            companyName: 'Teste2',
            symbol: 'T',
            value: 120,
          },
        },
      });
    });
  });
  describe('Test deleteUserStock method', () => {
    let dispatch = jest.fn();

    beforeEach(() => {
      dispatch = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should delete stock', () => {
      // act
      deleteUserStock({
        userStocks: mockedUserStocks,
        dispatch,
        stockCode: 'IET',
      });

      // expect
      expect(dispatch).toBeCalledWith({
        type: 'SET_USER_STOCKS',
        userStocks: {
          T: {
            value: 120,
          },
        },
      });
    });
    test("Should reset graph data when there isn't stock", () => {
      // act
      deleteUserStock({
        userStocks: {
          IET: {
            value: 100,
          },
        },
        dispatch,
        stockCode: 'IET',
      });

      // expect
      expect(dispatch).toHaveBeenLastCalledWith({
        graphData: [0, 0, 0],
        type: 'SET_SYSTEM_GRAPH',
      });
    });
  });
  describe('Test reorderList method', () => {
    let dispatch = jest.fn();

    beforeEach(() => {
      dispatch = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should reorder clicked stock in first position', () => {
      // act
      reorderList({userStocks: mockedUserStocks, stockCode: 'T', dispatch});

      expect(dispatch).toBeCalledWith({
        graphData: [0, 0, 0],
        type: 'SET_SYSTEM_GRAPH',
      });
      expect(dispatch).toHaveBeenLastCalledWith({
        type: 'SET_USER_STOCKS',
        userStocks: {T: {value: 120}, IET: {value: 100}},
      });
    });
  });
  describe('Test getStockStatus method', () => {
    test('Should return down status when old is greater than actual value', () => {
      // act
      const result = getStockStatus({old: 1, value: 0});
      // expect
      expect(result).toBe('down');
    });
    test('Should return up status when old is lesser than actual value', () => {
      // act
      const result = getStockStatus({old: 1, value: 2});
      // expect
      expect(result).toBe('up');
    });
    test('Should return nomral status when old actual value are equal', () => {
      // act
      const result = getStockStatus({old: 1, value: 1});
      // expect
      expect(result).toBe('normal');
    });
  });
  describe('Test updateGraphData method', () => {
    let dispatch = jest.fn();

    beforeEach(() => {
      dispatch = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should update graph with new value', () => {
      // act
      updateGraphData({
        userStocks: mockedUserStocks,
        key: 'IET',
        value: 200,
        graphData: [],
        dispatch,
      });

      //expect
      expect(dispatch).toBeCalledWith({
        graphData: [200],
        type: 'SET_SYSTEM_GRAPH',
      });
    });
  });
  describe('Test updateUserStocks method', () => {
    let dispatch = jest.fn();

    beforeEach(() => {
      dispatch = jest.fn();
    });

    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should update graph with new value', () => {
      // act
      updateUserStocks({
        userStocks: mockedUserStocks,
        key: 'IET',
        value: 200,
        dispatch,
      });
      //expect
      expect(dispatch).toBeCalledWith({
        type: 'SET_USER_STOCKS',
        userStocks: {IET: {status: 'up', value: 200}, T: {value: 120}},
      });
    });
  });
});
