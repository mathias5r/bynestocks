import {handleStocksBuffer} from '../src/services/buffer';

const mockedUserStocks = {
  IET: {
    value: 100,
  },
  T: {
    value: 120,
  },
};

describe('Test buffer service', () => {
  describe('Test handleStocksBuffer method', () => {
    let dispatch = jest.fn();
    beforeEach(() => {
      dispatch = jest.fn();
    });
    afterEach(() => {
      jest.clearAllMocks();
    });
    test('Should create key in buffer when it not exists', () => {
      // act
      const result = handleStocksBuffer({
        userStocks: mockedUserStocks,
        stocksBuffer: {},
        key: 'T',
        value: 100,
        dispatch,
      });

      expect(dispatch).toBeCalledWith({
        stocksBuffer: {T: [100]},
        type: 'SET_STOCKS_BUFFER',
      });
      expect(result).toBe(null);
    });
    test('Should push new value to buffer when buffer is not grater than user stocks', () => {
      // act
      const result = handleStocksBuffer({
        userStocks: mockedUserStocks,
        stocksBuffer: {T: [100]},
        key: 'T',
        value: 200,
        dispatch,
      });
      // expect
      expect(dispatch).toBeCalledWith({
        stocksBuffer: {T: [100, 200]},
        type: 'SET_STOCKS_BUFFER',
      });
      expect(result).toBe(null);
    });
    test('Should return average value when buffer is full', () => {
      // act
      const result = handleStocksBuffer({
        userStocks: mockedUserStocks,
        stocksBuffer: {T: [100, 200]},
        key: 'T',
        value: 300,
        dispatch,
      });
      // expect
      expect(dispatch).toBeCalledWith({
        stocksBuffer: {T: []},
        type: 'SET_STOCKS_BUFFER',
      });
      expect(result).toBe(150);
    });
  });
});
